/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-10 10:50:23
 * @Last Modified by:   Liudu Mac
 * @Last Modified time: 2018-12-01 14:27:57
 */

// const BASIC_URL = 'https://www.gsmgx.com'
const BASIC_URL = 'https://dev.wx.03in.net:8086'
export default function Login(resolve, reject) {

  wx.showLoading({
    title: '正在登录...',
    mask: true
  })

  wx.getUserInfo({
    lang: 'zh_CN',
    success: res => {
      console.log('res', res)

      getApp().globalData.UserInfo = JSON.parse(res.rawData)

      console.log('QrCode', getApp().globalData.QrCode)

      wx.request({
        url: BASIC_URL + '/member/Index/login',
        method: 'POST',
        data: {
          code: getApp().globalData.Code,
          qrcode: getApp().globalData.QrCode || '',
          userinfo: {
            encryptedData: res.encryptedData,
            iv: res.iv
          },
        },
        success: (r) => {
          console.log('Login Success', r)
          let _d = r.data
          getApp().globalData.UserData = _d.data
          getApp().globalData.Uid = _d.data.uid
          getApp().globalData.SessionId = _d.data.session_id
          getApp().globalData.isLogin = _d.loginState

          try {
            wx.setStorageSync('SessionId', _d.data.session_id)
          } catch (e) {
            throw error(r)
          }

          if (_d.data.phone == '' || _d.data.phone == '0') {
            wx.redirectTo({
              url: '/pages/phone/main'
            })
            reject()
          } else {
            resolve(_d)
          }
          wx.hideLoading()
        },

        fail: (e) => {
          wx.hideLoading()
          wx.reLaunch({
            url: '/pages/login/main'
          })
          reject(e)
        }
      })
    }
  })
}