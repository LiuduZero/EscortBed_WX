function formatNumber(n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

function formatTime(date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join('/')
  const t2 = [hour, minute, second].map(formatNumber).join(':')

  return `${t1} ${t2}`
}


export function timeToArr(time) {
  let t = time.split(':')
  let h = t[0]
  let m = t[1]
  let arr = []
  const date = new Date()
  let H = date.getHours()
  let M = date.getMinutes()

  if (M > 30) {
    H += 1
  }
  let _h = [],
    _m = ['00分', '30分']
  if (h >= H) {
    for (let i = H; i <= h; i++) {
      _h.push(i + '点')
    }

  } else {
    for (let i = H; i <= 24; i++) {
      _h.push(i + '点')
    }
    for (let i = 0; i <= h; i++) {
      _h.push(i < 10 ? '0' + i + '点' : i + '点')
    }
  }
  arr = [
    [..._h],
    [..._m]
  ]
  return arr
}

export function GetQueryString(url, name) {
  console.log('url = ' + url)
  console.log('name = ' + name)
  var reg = new RegExp('(^|&|/?)' + name + '=([^&|/?]*)(&|/?|$)', 'i')
  var r = url.substr(1).match(reg)
  if (r != null) {
    console.log('r = ' + r)
    console.log('r[2] = ' + r[2])
    return r[2]
  }
  return null;
}