/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-04 17:33:08
 * @Last Modified by:   Liudu Mac
 * @Last Modified time: 2018-12-01 14:27:49
 */

const BASIC_URL = 'https://www.gsmgx.com'
  // const BASIC_URL = 'https://dev.wx.03in.net:8086'

const createdNumer = function(type) {
  let _d = new Date()
  let _number = '',
    _y = _d.getFullYear(),
    _m = _d.getMonth() + 1,
    _dd = _d.getDate(),
    _h = _d.getHours(),
    _mm = _d.getMinutes(),
    _s = _d.getSeconds(),
    _ms = _d.getMilliseconds();

  if (_ms < 10) {
    _ms = '00' + _ms
  } else if (_ms < 100) {
    _ms = '0' + _ms
  }

  let _ts = [_m, _dd, _h, _mm, _s]
  for (let i = 0; i < _ts.length; i++) {
    if (_ts[i] < 10) {
      _ts[i] = '0' + _ts[i]
    }
  }
  _number = type + _y + '' + _ts[0] + '' + _ts[1] + '' + _ts[2] + '' + _ts[3] + '' + _ts[4] + '' + _ms + ''
  return _number
}

const Pay = function({
  type,
  label,
  money,
  isUnlock = false,
}, CallBack) {

  let _number = createdNumer(type)
  let _openId = getApp().globalData.UserData.openid

  wx.showLoading({
    title: '加载中...',
    mask: true
  })
  wx.request({
    url: BASIC_URL + '/pay/Wechat/prepay/type/' + type,
    method: 'POST',
    data: {
      openid: _openId,
      body: label,
      order_sn: _number,
      total_fee: money
        // total_fee: 0.01 //测试支付 0.01
    },
    success: (r) => {
      wx.hideLoading()
      let _r = r.data.data
      wx.requestPayment({
        'appId': _r.appId,
        'timeStamp': _r.timeStamp + '',
        'nonceStr': _r.nonceStr,
        'package': _r.package,
        'signType': _r.signType,
        'paySign': _r.paySign,
        'success': rs => {
          console.log('rs', rs)
          if (rs.errMsg == 'requestPayment:ok') {
            // if (CallBack != undefined && 'function' == typeof CallBack) {
            //   console.log(type, label, money, CallBack)
            //   CallBack.apply()
            // }

            wx.showToast({
              title: '正在充值...',
              icon: 'loading', // "success", "loading", "none"
              duration: 1000,
              mask: true,
            })

            setTimeout(() => {
              wx.request({
                url: BASIC_URL + '/pay/Index/getRechargeResult',
                method: 'POST',
                data: {
                  order_no: _number
                },
                success: res => {
                  if (res.data.status) {
                    if (CallBack != undefined && 'function' == typeof CallBack) {
                      CallBack()
                    }
                    if (!isUnlock) {
                      wx.redirectTo({
                        url: '/pages/result/main?n=1'
                      })
                    }
                  } else {
                    if (!isUnlock) {
                      wx.redirectTo({
                        url: '/pages/result/main?n=0'
                      })
                    }
                  }
                }
              })
            }, 1200)
          }
        },
        'fail': e => {
          console.log('e', e)
        },
        'complete': c => {
          console.log('c', c)
        }
      })
    },
    fail: (e) => {
      wx.hideLoading()
    }
  })
}

export default Pay