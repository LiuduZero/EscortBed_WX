/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-17 10:19:51
 * @Last Modified by:   Liudu Mac
 * @Last Modified time: 2018-12-01 14:21:07
 */

const BaseUrl = process.env.BASIC_URL
  // const BaseUrl = 'https://dev.wx.03in.net:8086'

let Wxhttp = function(opt) {

  let Loading = new Loading()

  this.showLoading = () => {
    if (opt.isLoading) {
      wx.showLoading({
        title: '正在加载中',
        mask: true
      })
    } else {
      wx.showNavigationBarLoading()
    }
  }

  this.get = opt => {
    this.showLoading()

    const promise = new Promise((resolve, reject) => {
      wx.request({
        url: BaseUrl + url,
        data: data,
        method: 'GET',
        success: r => {
          if (r.statusCode) {
            resolve(r.data);
          } else {
            resolve('请求失败');
          }
        },
        fail: e => {
          reject('请求错误')
        },
        complete: function() {
          if (opt.isLoading) {
            wx.hideLoading();
          } else {
            wx.hideNavigationBarLoading()
          }
        }
      })

    })
    return promise
  }

  this.post = (url, data, opt) => {

    this.showLoading()

    const promise = new Promise((resolve, reject) => {
      wx.request({
        url: BaseUrl + url,
        data: data,
        method: 'POST',
        success: r => {
          if (r.statusCode) {
            resolve(r.data);
          } else {
            resolve('请求失败');
          }
        },
        fail: e => {
          reject('请求错误')
        },
        complete: function() {
          if (opt.isLoading) {
            wx.hideLoading();
          } else {
            wx.hideNavigationBarLoading()
          }
        }
      })

    })
    return promise
  }

}



export default new Wxhttp()