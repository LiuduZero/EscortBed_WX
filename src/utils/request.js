/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-01 16:53:42
 * @Last Modified by:   Liudu
 * @Last Modified time: 2019-04-02 15:45:16
 */
const Fly = require('flyio/dist/npm/wx')
const Qs = require('qs');
const fly = new Fly

let IS_LOADING = null
let SessionId = ''

console.log('process=>', process)

fly.config = {
    baseURL: 'https://www.gsmgx.com',
    // baseURL: 'https://dev.wx.03in.net:8086',

    timeout: 8000, //超时时间，为0时则无超时限制
  }
  //添加请求拦截器
fly.interceptors.request.use(config => {
  let _Gd = getApp().globalData
    // 登录失败，重新登录
    // if (!getApp().globalData.SessionId) {
    //   try {
    //     let _val = wx.getStorageSync('SessionId')
    //     if (!_val) {
    //       wx.showModal({
    //         title: '提示',
    //         content: '请重新登录',
    //         showCancel: false,
    //         confirmText: '确定',
    //         confirmColor: '#3CC51F',
    //         success: (res) => {
    //           if (res.confirm) {
    //             wx.reLaunch({
    //               url: '/pages/login/main'
    //             })
    //           }
    //         }
    //       })
    //       return promise.reject()
    //     } else {
    //       SessionId = getApp().globalData.SessionId
    //     }
    //   } catch (e) {
    //     throw error(r)
    //   }
    // }

  if (!getApp().globalData.SessionId) {
    return Promise.reject()
  } else {
    if (SessionId == '') {
      SessionId = getApp().globalData.SessionId
    }
  }

  config.headers['AccessToken'] = SessionId

  //给所有请求添加自定义header
  config.headers["X-Tag"] = "flyio";
  //打印出请求体
  if (config.method === 'GET') {
    config = Object.assign(config, config.body)
    config.body = {}
  } else {
    config.body = Qs.stringify(config.body)
  }
  IS_LOADING = config.isLoading
  if (IS_LOADING) {
    wx.showLoading({
      title: '正在加载中...',
      mask: true
    })
  } else {
    wx.showNavigationBarLoading()
  }

  //return Promise.reject(new Error(""))
  return config
    //可以显式返回config, 也可以不返回，没有返回值时拦截器中默认返回config

})

//添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
  (response) => {
    if (IS_LOADING) {
      wx.hideLoading()
    } else {
      wx.hideNavigationBarLoading()
    }
    //只将请求结果的data字段返回
    return response.data
  },
  (err) => {
    if (IS_LOADING) {
      wx.hideLoading()
    } else {
      wx.hideNavigationBarLoading()
    }

    if (!SessionId) {
      wx.showModal({
        title: '提示',
        content: '请重新登录',
        showCancel: false,
        confirmText: '确定',
        confirmColor: '#3CC51F',
        success: (res) => {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/main'
            })
          }
        }
      })
    } else {
      wx.showToast({
        title: err.message.includes('timeout') ? '请求超时' : '网络请求错误',
        icon: 'none',
        image: '../../static/images/error_64.png',
        mask: true,
      })
    }

    //发生网络错误后会走到这里
    return Promise.resolve()
  }
)

export default fly