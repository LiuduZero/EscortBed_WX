import Vue from 'vue'
import App from './App'
import Https from './utils/request.js'

Vue.prototype.$Https = Https
Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue(App)
app.$mount()

//主色 #00758E

export default {
  // 这个字段走 app.json
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去

    // 可有可无???
    pages: ['^pages/index/main'],
    window: {
      backgroundColorTop: '#f7f8f9',
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#00758E',
      backgroundColorBottom: '#f7f8f9',
      navigationBarTitleText: '高斯美共享陪护床',
      navigationBarTextStyle: 'white'
    },
    tabBar: {
      color: '#999',
      selectedColor: '#00758E',
      list: [{
        pagePath: 'pages/index/main',
        text: '首页',
        iconPath: 'static/images/tab_home.png',
        selectedIconPath: 'static/images/tab_home_2.png'
      }, {
        pagePath: 'pages/person/main',
        text: '个人中心',
        iconPath: 'static/images/tab_user.png',
        selectedIconPath: 'static/images/tab_user_2.png'
      }]
    },
  }
}