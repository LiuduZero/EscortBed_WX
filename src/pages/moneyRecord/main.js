/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-30 17:16:18
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-08-18 16:44:21
 */
import Vue from 'vue'
import Record from './index'

const app = new Vue(Record)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '账户流水',
    enablePullDownRefresh: true,
    onReachBottomDistance: 10
  }
}