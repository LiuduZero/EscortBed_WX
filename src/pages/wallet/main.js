/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-27 16:48:42
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-07-27 16:54:48
 */
import Vue from 'vue'
import Login from './index'

const app = new Vue(Login)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '钱包'
  }
}