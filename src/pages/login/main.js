/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-26 09:49:55
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-07-26 14:58:07
 */
import Vue from 'vue'
import Login from './index'

const app = new Vue(Login)
app.$mount()

export default {
    config: {
        navigationBarTitleText: '快速登录'
    }
}