/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-30 16:29:48
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-08-17 17:20:51
 */
import Vue from 'vue'
import Record from './index'

const app = new Vue(Record)
app.$mount()

export default {
    config: {
        navigationBarTitleText: '我的订单',
        enablePullDownRefresh: true,
        onReachBottomDistance: 10
    }
}