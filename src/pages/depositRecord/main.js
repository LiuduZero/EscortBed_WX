/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-05 14:47:21
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-09-14 15:23:03
 */
import Vue from 'vue'
import Record from './index'

const app = new Vue(Record)
app.$mount()

export default {
    config: {
        navigationBarTitleText: '信用分明细',
        enablePullDownRefresh: true,
        onReachBottomDistance: 10,
    }
}