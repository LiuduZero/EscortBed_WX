/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-05 10:49:33
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import Intro from './index'

const app = new Vue(Intro)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '信用分说明',

  }
}