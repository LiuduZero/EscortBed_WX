/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-04 16:35:36
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import Deposit from './index'

const app = new Vue(Deposit)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '缴纳押金',

  }
}