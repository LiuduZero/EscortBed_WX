/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-30 15:24:19
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import Recharge from './index'

const app = new Vue(Recharge)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '充值'
  }
}