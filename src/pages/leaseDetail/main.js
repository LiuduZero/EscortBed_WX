/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-31 10:36:53
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import Detail from './index'

const app = new Vue(Detail)
app.$mount()

export default {
    config: {
        navigationBarTitleText: '订单详情',
    }
}