/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-03 09:28:59
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import Result from './index'

const app = new Vue(Result)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '支付结果',

  }
}