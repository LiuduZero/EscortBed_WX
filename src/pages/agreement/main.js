/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-17 16:12:48
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-08-17 16:13:36
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '充值协议',

  }
}