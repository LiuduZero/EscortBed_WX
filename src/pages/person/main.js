/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-27 17:04:28
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-07-27 17:04:46
 */
import Vue from 'vue'
import Login from './index'

const app = new Vue(Login)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '个人中心'
  }
}