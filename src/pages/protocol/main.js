/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-17 16:12:48
 * @Last Modified by:   Liudu Mac
 * @Last Modified time: 2018-12-01 14:49:35
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '开锁协议',

  }
}