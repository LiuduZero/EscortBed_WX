/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-06 17:18:12
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '开锁'
  }
}