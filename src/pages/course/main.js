/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-20 17:12:00
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-08-20 18:15:01
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '使用帮助',

  }
}