/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-31 11:30:15
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-08-01 17:00:19
 */
import Vue from 'vue'
import Work from './index'

const app = new Vue(Work)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '故障反馈',
  }
}