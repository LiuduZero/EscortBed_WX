/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-07-27 09:24:22
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-07-27 20:26:02
 */
import Vue from 'vue'
import Settings from './index'

const app = new Vue(Settings)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '设置'
  }
}