/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-09-06 11:34:09
 * @Last Modified by:   Liudu
 * @Last Modified time: 2018-09-06 14:57:58
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '自定义页面'
  }
}