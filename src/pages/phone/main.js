/**
 * @Author: Liudu (***@***.***)
 * @Date:   2018-08-24 14:51:34
 * @Last Modified by:   Liudu
 * @Last Modified time: {{last_modified_time}}
 */
import Vue from 'vue'
import Phone from './index'

const app = new Vue(Phone)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '绑定手机号'
  }
}